package com.patelheggere.listfeed.data;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by dell on 10/28/2017.
 */

public class LikeModel implements Serializable {
    public LikeModel(){}
    String key,LikedBy,LikedByName;
    Map<String,String> LikedOn;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLikedBy() {
        return LikedBy;
    }

    public void setLikedBy(String likedBy) {
        LikedBy = likedBy;
    }

    public String getLikedByName() {
        return LikedByName;
    }

    public void setLikedByName(String likedByName) {
        LikedByName = likedByName;
    }

    public Map<String, String> getLikedOn() {
        return LikedOn;
    }

    public void setLikedOn(Map<String, String> likedOn) {
        LikedOn = likedOn;
    }
}
